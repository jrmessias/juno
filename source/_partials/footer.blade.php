<!-- Main footer -->
<footer
        class="flex items-center justify-between p-4 border-t border-slate-200 dark:border-slate-700 bg-slate-100 text-slate-500 dark:bg-slate-900/50"
>
    <div>{{ $page->id }} &copy; 2022</div>
    <div class="flex items-end">
        <svg aria-hidden="true" class="w-6 h-6 text-slate-500" xmlns="http://www.w3.org/2000/svg" width="24"
             height="24" viewBox="0 0 24 24" fill="currentColor">
            <path class="bg-slate-100"
                  d="m7.375 16.781 1.25-1.562L4.601 12l4.024-3.219-1.25-1.562-5 4a1 1 0 0 0 0 1.562l5 4zm9.25-9.562-1.25 1.562L19.399 12l-4.024 3.219 1.25 1.562 5-4a1 1 0 0 0 0-1.562l-5-4zm-1.649-4.003-4 18-1.953-.434 4-18z"></path>
        </svg>
        <a href="https://github.com/{{ $page->github }}" target="_blank"
           class="text-blue-500 hover:underline pl-2">{{ $page->author }}</a>
    </div>
</footer>
