<nav aria-label="Secondary" class="hidden space-x-2 md:flex md:items-center">
    <!-- Toggle dark theme button -->
    <x-button-header click="toggleTheme" title="Toggle theme">
        <svg
                class="w-7 h-7 fill-slate-500 hidden dark:block"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                aria-hidden="true"
        >
            <path d="M6.993 12c0 2.761 2.246 5.007 5.007 5.007s5.007-2.246 5.007-5.007S14.761 6.993 12 6.993 6.993 9.239 6.993 12zM12 8.993c1.658 0 3.007 1.349 3.007 3.007S13.658 15.007 12 15.007 8.993 13.658 8.993 12 10.342 8.993 12 8.993zM10.998 19h2v3h-2zm0-17h2v3h-2zm-9 9h3v2h-3zm17 0h3v2h-3zM4.219 18.363l2.12-2.122 1.415 1.414-2.12 2.122zM16.24 6.344l2.122-2.122 1.414 1.414-2.122 2.122zM6.342 7.759 4.22 5.637l1.415-1.414 2.12 2.122zm13.434 10.605-1.414 1.414-2.122-2.122 1.414-1.414z"></path>
        </svg>
        <svg
                class="w-7 h-7 fill-slate-500 dark:hidden"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                aria-hidden="true"
        >
            <path d="M20.742 13.045a8.088 8.088 0 0 1-2.077.271c-2.135 0-4.14-.83-5.646-2.336a8.025 8.025 0 0 1-2.064-7.723A1 1 0 0 0 9.73 2.034a10.014 10.014 0 0 0-4.489 2.582c-3.898 3.898-3.898 10.243 0 14.143a9.937 9.937 0 0 0 7.072 2.93 9.93 9.93 0 0 0 7.07-2.929 10.007 10.007 0 0 0 2.583-4.491 1.001 1.001 0 0 0-1.224-1.224zm-2.772 4.301a7.947 7.947 0 0 1-5.656 2.343 7.953 7.953 0 0 1-5.658-2.344c-3.118-3.119-3.118-8.195 0-11.314a7.923 7.923 0 0 1 2.06-1.483 10.027 10.027 0 0 0 2.89 7.848 9.972 9.972 0 0 0 7.848 2.891 8.036 8.036 0 0 1-1.484 2.059z"></path>
        </svg>
    </x-button-header>

    <!-- Left Panel button -->
    <x-button-header click="openLeftPanel" title="Left Panel">
        <svg
                class="w-7 h-7 fill-slate-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                aria-hidden="true"
        >
            <path d="M4 6h2v12H4zm4 7h8.586l-4.293 4.293 1.414 1.414L20.414 12l-6.707-6.707-1.414 1.414L16.586 11H8z"></path>
        </svg>
    </x-button-header>

    <!-- Right panel button -->
    <x-button-header click="openRightPanel" title="Right Panel">
        <svg
                class="w-7 h-7 fill-slate-500"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                aria-hidden="true"
        >
            <path d="M18 6h2v12h-2zm-2 5H7.414l4.293-4.293-1.414-1.414L3.586 12l6.707 6.707 1.414-1.414L7.414 13H16z"></path>
        </svg>
    </x-button-header>

    <!-- User avatar button -->
    <div class="relative" x-data="{ open: false }">
        <button
                @click="open = !open; $nextTick(() => { if(open){ $refs.userMenu.focus() } })"
                type="button"
                aria-haspopup="true"
                :aria-expanded="open ? 'true' : 'false'"
                class="transition-opacity duration-200 rounded-full dark:opacity-75 dark:hover:opacity-100 focus:outline-none"
        >
            <span class="sr-only">User menu</span>
            <img class="w-10 h-10 rounded-full" src="https://ui-avatars.com/api/?name={{ $page->author }}"
                 alt="{{ $page->author }}"/>
        </button>

        <!-- User dropdown menu -->
        <div
                x-show="open"
                x-ref="userMenu"
                x-transition:enter="transition-all transform ease-out"
                x-transition:enter-start="translate-y-1/2 opacity-0"
                x-transition:enter-end="translate-y-0 opacity-100"
                x-transition:leave="transition-all transform ease-in"
                x-transition:leave-start="translate-y-0 opacity-100"
                x-transition:leave-end="translate-y-1/2 opacity-0"
                @click.away="open = false"
                @keydown.escape="open = false"
                class="absolute right-0 w-48 py-1 bg-slate-100 rounded-lg shadow-lg top-12 dark:bg-slate-900 focus:outline-none"
                tabindex="-1"
                role="menu"
                aria-orientation="vertical"
                aria-label="User menu"
        >
            @foreach ($page->menuUser as $menuUser)
                @if($menuUser->link == "logout")
                    <div class="w-full h-px bg-slate-300 dark:bg-slate-700"></div>
                @endif
                <a
                        href="{{ $menuUser->link }}"
                        role="menuitem"
                        class="block px-4 py-2 text-sm text-slate-500 transition-colors hover:bg-slate-200 hover:text-slate-700 dark:text-slate-500 dark:hover:bg-slate-700 dark:hover:text-slate-400"
                >
                    <i class="mdi {{ $menuUser->icon }} pr-1"></i>
                    {{ $menuUser->name }}
                </a>
            @endforeach
        </div>
    </div>
</nav>
