    <!-- Navbar -->
    <header class="relative bg-slate-100 text-slate-500 dark:bg-slate-900">
        <div class="flex items-center justify-between p-2 border-b dark:border-slate-700">
            <!-- Mobile menu button -->
            <button
                    @click="isMobileMainMenuOpen = !isMobileMainMenuOpen"
                    class="p-1 transition-colors duration-200 rounded-md md:hidden outline-none"
            >
                <span class="sr-only">Open main menu</span>
                <span aria-hidden="true">
                  <svg
                          class="w-8 h-8  fill-slate-500"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                  >
                      <path d="M4 6h16v2H4zm0 5h16v2H4zm0 5h16v2H4z"></path>
                  </svg>
                </span>
            </button>

            <!-- Brand -->
            <a
                    href="/"
                    class="inline-block text-2xl font-bold tracking-wider"
            >
                {{ $page->siteName }}
            </a>

            <!-- Mobile sub menu button -->
            <button
                    @click="isMobileSubMenuOpen = !isMobileSubMenuOpen"
                    class="p-1 transition-colors duration-200 rounded-md md:hidden outline-none focus:bg-none"
            >
                <span class="sr-only">Open sub menu</span>
                <span aria-hidden="true">
                  <svg
                          class="w-8 h-8 fill-slate-500"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                  >
                      <path d="M12 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0-6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 12c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path>
                  </svg>
                </span>
            </button>

            <!-- Desktop Right buttons -->
            @include('_partials.desktop-right-buttons')

            <!-- Mobile sub menu -->
            @include('_partials.mobile-sub-menu')
        </div>
        <!-- Mobile main menu -->
        @include('_partials.mobile-main-menu')
    </header>
