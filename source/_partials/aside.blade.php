<aside class="flex-shrink-0 hidden text-white w-64 bg-slate-100 border-r dark:border-slate-700 dark:bg-slate-900 md:block"
       :class="{ 'w-12': !isSidebarOpen }">
    <div class="flex flex-col h-full" @mouseover.away="{isSidebarOpen == false ? !isSidebarOpen : isSidebarOpen}">
        <div class="py-6 px-2 flex items-center justify-center">
            <div :class="{ 'hidden' : !isSidebarOpen }">
                <a href="/">
                    <x-brand></x-brand>
                </a>
            </div>
            {{--            <button id="sidebar-toggle" @click="toggleSidebar" type="button"--}}
            {{--                    class="transition delay-200 border-0 text-slate-500 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-slate-700 focus:outline-none rounded-full text-sm p-1 w-6 h-6 dark:bg-slate-800 cursor-pointer">--}}
            {{--                <svg xmlns="http://www.w3.org/2000/svg" class="w-full fill-slate-500" viewBox="0 0 24 24"--}}
            {{--                     :class="{ 'rotate-180 fill-blue-500': !isSidebarOpen }">--}}
            {{--                    <path d="m12.707 7.707-1.414-1.414L5.586 12l5.707 5.707 1.414-1.414L8.414 12z"></path>--}}
            {{--                    <path d="M16.293 6.293 10.586 12l5.707 5.707 1.414-1.414L13.414 12l4.293-4.293z"></path>--}}
            {{--                </svg>--}}
            {{--            </button>--}}
        </div>
        <div class="box-search" :class="{ 'hidden' : isSidebarOpen }">
            <div class="w-full h-[1px] bg-slate-300 dark:bg-slate-700"></div>
            <div class="p-4 flex items-center justify-center">
                <x-input-icon placeholder="Search" class="mb-0" type="search">
                    <path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path>
                    <path d="M11.412 8.586c.379.38.588.882.588 1.414h2a3.977 3.977 0 0 0-1.174-2.828c-1.514-1.512-4.139-1.512-5.652 0l1.412 1.416c.76-.758 2.07-.756 2.826-.002z"></path>
                </x-input-icon>
            </div>
            <div class="w-full h-[1px] bg-slate-300 dark:bg-slate-700"></div>
        </div>
        <!-- Sidebar links -->
        <nav aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">
            @foreach ($page->menuSidebar as $menuSidebar)
                <div x-data="{ isActive: {{ $loop->first ? 'true' : 'false' }}, open: {{ $loop->first ? 'true' : 'false' }}}">
                    <a
                            href="#"
                            @click="$event.preventDefault(); open = !open"
                            class="flex items-center p-2 transition-colors rounded-md hover:bg-slate-200/50 dark:hover:bg-slate-700/50"
                            :class="{'bg-primary-100 dark:bg-primary': isActive || open}"
                            role="button"
                            aria-haspopup="true"
                            :aria-expanded="(open || isActive) ? 'true' : 'false'"
                    >
                  <span aria-hidden="true">
                    <svg
                            class="w-5 h-5 fill-slate-500"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                    >
                     {!! $menuSidebar->icon !!}
                    </svg>
                  </span>
                        <span class="ml-2 text-sm text-slate-700 dark:text-slate-400 font-semibold">{{ $menuSidebar->title }}</span>
                        <span class="ml-auto" aria-hidden="true">
                    <!-- active class 'rotate-180' -->
                    <svg
                            class="w-4 h-4 transition-transform transform fill-slate-500"
                            :class="{ 'rotate-180': open }"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                    >
                        <path d="M16.293 9.293 12 13.586 7.707 9.293l-1.414 1.414L12 16.414l5.707-5.707z"></path>
                    </svg>
                  </span>
                    </a>
                    <div role="menu" x-show="open" class="mt-2 space-y-2 px-4" aria-label="{{ $menuSidebar->title }}">
                        @foreach ($menuSidebar->itens as $menuItem)
                            <a
                                    href="{{ $menuItem->url }}"
                                    role="menuitem"
                                    class="flex justify-between p-2 text-sm transition-colors duration-200 rounded-lg">
                                <span class="flex">
                                    <svg
                                            class="w-5 h-5 fill-slate-500 mr-2"
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                    >
                     {!! $menuSidebar->icon !!}
                                    </svg>
                                <span class="whitespace-nowrap text-slate-500 hover:text-slate-700 dark:hover:text-slate-400">{{ $menuItem->title }}</span>
                                </span>
                                @if($loop->first)
                                    <span class="inline-flex items-center justify-center px-2 ml-3 text-sm font-medium text-blue-800 bg-blue-200 rounded-full dark:bg-blue-700 dark:text-blue-300">Pro</span>
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
            @endforeach
        <!-- CT -->
            {{--            <div class="cta" :class="{ 'hidden' : isSidebarOpen }">--}}
            <x-side-menu-cta></x-side-menu-cta>
            {{--            </div>--}}
        </nav>

        <!-- Sidebar footer -->
        {{--        <div class="flex-shrink-0 px-2 py-4 space-y-2">--}}
        {{--            <x-side-menu-cta></x-side-menu-cta>--}}
        {{--            <div class="flex w-full justify-center">--}}
        {{--            <x-brand></x-brand>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>
</aside>
