@extends('_layouts.master')

@section('body')
    @include("_partials.content-title", ["title" => "CRUD - RUD"])
    <div class="bg-slate-300 dark:bg-slate-900/50 border-l-4 rounded border-blue-500 p-4 m-4 text-right shadow ">
        <x-button-a class="bg-red-500 hover:bg-red-700 text-slate-100">
            <div class="flex items-end">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="fill-slate-100">
                    <path d="m20 8-6-6H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8zM9 19H7v-9h2v9zm4 0h-2v-6h2v6zm4 0h-2v-3h2v3zM14 9h-1V4l5 5h-4z"></path>
                </svg>
                <span>&nbsp; Report</span>
            </div>
        </x-button-a>
        <x-button-a class="bg-blue-500 hover:bg-blue-700 text-slate-100" href="/pages/crud/create">
            <div class="flex items-end">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="fill-slate-100">
                    <path d="M12 2C6.486 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.514 2 12 2zm5 11h-4v4h-2v-4H7v-2h4V7h2v4h4v2z"></path>
                </svg>
                <span>&nbsp; New</span>
            </div>
        </x-button-a>
    </div>
    <div class="m-4">
        <div class="overflow-x-auto shadow-md rounded-t-lg bg-slate-300 dark:bg-slate-900/50 px-4 py-3 flex items-center justify-between">
            <div class="mt-1 flex rounded-md shadow-sm">
                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-slate-300 bg-slate-50 text-slate-500 text-sm dark:bg-slate-700/50 dark:border-slate-700">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="fill-slate-500">
                        <path d="M10 18a7.952 7.952 0 0 0 4.897-1.688l4.396 4.396 1.414-1.414-4.396-4.396A7.952 7.952 0 0 0 18 10c0-4.411-3.589-8-8-8s-8 3.589-8 8 3.589 8 8 8zm0-14c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z"></path>
                        <path d="M11.412 8.586c.379.38.588.882.588 1.414h2a3.977 3.977 0 0 0-1.174-2.828c-1.514-1.512-4.139-1.512-5.652 0l1.412 1.416c.76-.758 2.07-.756 2.826-.002z"></path>
                    </svg>
                </span>
                <input type="text" name="company-website" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-slate-300 dark:bg-slate-800 dark:border-slate-700 placeholder:text-slate-500" placeholder="Search">
            </div>
        </div>
        <div class="flex flex-col">
            <div class="overflow-x-auto">
                <div class="inline-block min-w-full align-middle dark:bg-slate-800">
                    <div class="overflow-hidden">
                        <table class="min-w-full divide-y divide-slate-200 table-fixed dark:divide-slate-700">
                            @php
                                $products = [
                                    [
                                        'name' => 'Apple Imac 27',
                                        'type' => 'Desktop PC',
                                        'price' => '$1999'
                                    ],
                                    [
                                        'name' => 'Apple MacBook Pro 17"',
                                        'type' => 'Laptop',
                                        'price' => '$2999'
                                    ],
                                    [
                                        'name' => 'iPhone 13 Pro',
                                        'type' => 'Phone',
                                        'price' => '$999'
                                    ],
                                    [
                                        'name' => 'Apple Magic Mouse 2',
                                        'type' => 'Accessories',
                                        'price' => '$99'
                                    ],
                                    [
                                        'name' => 'Apple Watch Series',
                                        'type' => 'Accessories',
                                        'price' => '$599'
                                    ],
                                ];
                            @endphp
                            <thead class="bg-slate-300 dark:bg-slate-900/50">
                            <tr>
                                <th scope="col" class="p-4">
                                    <div class="flex items-center">
                                        <input id="checkbox-search-all" type="checkbox"
                                               class="w-4 h-4 text-blue-600 bg-slate-100 rounded border-slate-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-slate-800 focus:ring-2 dark:bg-slate-700 dark:border-slate-600">
                                        <label for="checkbox-search-all" class="sr-only">checkbox</label>
                                    </div>
                                </th>
                                <th scope="col"
                                    class="py-3 px-6 text-xs font-medium tracking-wider text-left text-slate-700 uppercase dark:text-slate-400">
                                    Product Name
                                </th>
                                <th scope="col"
                                    class="py-3 px-6 text-xs font-medium tracking-wider text-left text-slate-700 uppercase dark:text-slate-400">
                                    Category
                                </th>
                                <th scope="col"
                                    class="py-3 px-6 text-xs font-medium tracking-wider text-left text-slate-700 uppercase dark:text-slate-400">
                                    Price
                                </th>
                                <th scope="col"
                                    class="py-3 px-6 text-xs font-medium tracking-wider text-left text-slate-700 uppercase dark:text-slate-400">
                                    Position
                                </th>
                                <th scope="col"
                                    class="py-3 px-6 text-xs font-medium tracking-wider text-left text-slate-700 uppercase dark:text-slate-400">
                                    Published
                                </th>
                                <th scope="col" class="p-4">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-slate-100 divide-y divide-slate-200 dark:bg-slate-800 dark:divide-slate-700">
                            @foreach($products as $product)
                                <tr class="hover:bg-slate-200 dark:hover:bg-slate-700">
                                    <td class="p-2 w-4">
                                        <div class="flex items-center">
                                            <input id="checkbox-search-1" type="checkbox"
                                                   class="w-4 h-4 text-blue-600 bg-slate-100 rounded border-slate-300 focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-slate-800 focus:ring-2 dark:bg-slate-700 dark:border-slate-600">
                                            <label for="checkbox-search-1" class="sr-only">checkbox</label>
                                        </div>
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium text-slate-900 whitespace-nowrap dark:text-slate-100">
                                        {{ $product['name'] }}
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium text-slate-500 whitespace-nowrap dark:text-slate-100">
                                        {{ $product['type'] }}
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium text-slate-900 whitespace-nowrap dark:text-slate-100">
                                        {{ $product['price'] }}
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium text-slate-900 whitespace-nowrap dark:text-slate-100 flex items-center">
                                        <span class="inline-flex items-center justify-center px-2 ml-3 text-sm font-medium text-purple-800 bg-purple-200 rounded-full dark:bg-purple-700 dark:text-purple-300 mr-1">{{ $loop->index }}</span>
                                        <x-button-small class="p-1 bg-purple-500 hover:bg-purple-700 text-slate-100 mr-1">
                                            <div class="flex items-end">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     class="fill-slate-100">
                                                    <path d="m6.293 13.293 1.414 1.414L12 10.414l4.293 4.293 1.414-1.414L12 7.586z"></path>
                                                </svg>
                                            </div>
                                        </x-button-small>
                                        <x-button-small class="p-1 bg-purple-500 hover:bg-purple-700 text-slate-100">
                                            <div class="flex items-end">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     class="fill-slate-100">
                                                    <path d="M16.293 9.293 12 13.586 7.707 9.293l-1.414 1.414L12 16.414l5.707-5.707z"></path>
                                                </svg>
                                            </div>
                                        </x-button-small>
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium whitespace-nowrap">
                                        <span class="inline-flex items-center justify-center px-2 ml-3 text-sm font-medium rounded-full mr-1 {{ $loop->even ? "text-green-800 bg-green-200 dark:bg-green-700 dark:text-green-300" : "text-red-800 bg-red-200 dark:bg-red-700 dark:text-red-300"}}">{{ $loop->even ? "Yes" : "No" }}</span>
                                        @if($loop->even)
                                            <x-button-a-small class="p-1 text-slate-100 bg-green-500 hover:bg-green-700">
                                                <span>Yes</span>
                                            </x-button-a-small>
                                        @else
                                            <x-button-a-small class="p-1 text-slate-100 bg-red-500 hover:bg-red-700">
                                                <span>No</span>
                                            </x-button-a-small>
                                        @endif
                                    </td>
                                    <td class="py-2 px-6 text-sm font-medium text-right whitespace-nowrap">
                                        <x-button-a-small class="p-1 bg-stone-500 hover:bg-stone-700 text-slate-100">
                                            <div class="flex items-end">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     class="fill-slate-100">
                                                    <path d="M12 5c-7.633 0-9.927 6.617-9.948 6.684L1.946 12l.105.316C2.073 12.383 4.367 19 12 19s9.927-6.617 9.948-6.684l.106-.316-.105-.316C21.927 11.617 19.633 5 12 5zm0 11c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"></path>
                                                    <path d="M12 10c-1.084 0-2 .916-2 2s.916 2 2 2 2-.916 2-2-.916-2-2-2z"></path>
                                                </svg>
                                            </div>
                                        </x-button-a-small>
                                        <x-button-a-small class="p-1 bg-blue-500 hover:bg-blue-700 text-slate-100">
                                            <div class="flex items-end">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     class="fill-slate-100">
                                                    <path d="m16 2.012 3 3L16.713 7.3l-3-3zM4 14v3h3l8.299-8.287-3-3zm0 6h16v2H4z"></path>
                                                </svg>
                                            </div>
                                        </x-button-a-small>
                                        <x-button-a-small class="p-1 bg-red-500 hover:bg-red-700 text-slate-100">
                                            <div class="flex items-end">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                     class="fill-slate-100">
                                                    <path d="m16.192 6.344-4.243 4.242-4.242-4.242-1.414 1.414L10.535 12l-4.242 4.242 1.414 1.414 4.242-4.242 4.243 4.242 1.414-1.414L13.364 12l4.242-4.242z"></path>
                                                </svg>
                                            </div>
                                        </x-button-a-small>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="overflow-x-auto shadow-md rounded-b-lg bg-slate-300 dark:bg-slate-900/50 px-4 py-3 flex items-center justify-between">
            <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                <div>
                    <p class="text-sm text-slate-700 dark:text-slate-100">
                        Showing
                        <span class="font-medium">1</span>
                        to
                        <span class="font-medium">10</span>
                        of
                        <span class="font-medium">97</span>
                        results
                    </p>
                </div>
                <div>
                    <nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                        <a href="#"
                           class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-slate-300 bg-slate-100 hover:bg-slate-50 text-sm font-medium text-slate-500 dark:bg-slate-700 dark:hover:bg-slate-800 dark:border-slate-600">
                            <span class="sr-only">Previous</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 class="h-5 w-5 fill-slate-500 dark:fill-slate-100">
                                <path d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"/>
                            </svg>
                        </a>
                        @for ($i = 1; $i <= 10; $i++)
                            <a href="#" aria-current="page"
                               class="{{ $i == 1 ? "z-10 bg-blue-50 border-blue-500 text-blue-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium dark:bg-blue-900 dark:text-blue-200" : "bg-slate-100 border-slate-300 text-slate-500 hover:bg-slate-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium dark:bg-slate-700 dark:hover:bg-slate-800 dark:border-slate-600" }}">
                                {{ $i == 3 ? "..." : $i }}
                            </a>
                            @if($i == 3)
                                @php($i = 7)
                            @endif
                        @endfor
                        <a href="#"
                           class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-slate-300 bg-slate-100 hover:bg-slate-50 text-sm font-medium text-slate-500 dark:bg-slate-700 dark:hover:bg-slate-800 dark:border-slate-600">
                            <span class="sr-only">Next</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 class="h-5 w-5 fill-slate-500 dark:fill-slate-100">
                                <path d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"></path>
                            </svg>
                        </a>
                    </nav>
                </div>
            </div>
        </div>
@endsection
