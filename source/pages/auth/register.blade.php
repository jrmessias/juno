@extends('_layouts.auth')

@section('body')
    <main>
        <div class="h-1 bg-slate-400 dark:bg-slate-700 rounded-t-lg mt-4"></div>
        <div class="w-full max-w-sm px-4 py-6 bg-white rounded-b-lg dark:bg-slate-900">
            <h1 class="text-xl font-semibold text-center text-slate-900 dark:text-slate-200">Register</h1>

            <form action="#" class="mt-4">
                {{--                <x-label for="email" label="E-mail"></x-label>--}}
                <x-input placeholder="Username" required="" name="username"></x-input>
                <x-input type="email" placeholder="E-mail address" required=""></x-input>
                <x-input type="password" placeholder="Password" minlength="8" required=""></x-input>
                <x-input type="password" placeholder="Confirm Password" minlength="8" required=""></x-input>
                <div class="flex items-center justify-between mt-4 mb-4">
                    <!-- Remember me toggle -->
                    <label class="inline-flex items-center">
                        <input type="checkbox" class="appearance-none checked:bg-green-500 hover:bg-slate-400 focus:checked:bg-slate-400 focus:bg-slate-400 focus:ring-0 focus:text-slate-400 rounded">
{{--                               class="default:ring-0 appearance-none w-5 h-5 bg-slate-200 border-0 rounded-md dark:checked:bg-green-500 checked:bg-green-500 checked:shadow-green-600/50 hover:bg-green-600 hover:text-green-600 hover:border-0 focus:bg-slate-300 focus:text-green-500 focus:ring-0 focus:outline-none before:shadow-none"/>--}}
                        <span class="ml-3 text-sm font-normal text-slate-900 dark:text-slate-500">I accept the <a href="/pages/auth/terms-and-conditions" class="text-sm text-blue-600 hover:underline">Terms and Conditions</a></span>
                    </label>
                </div>
                <div>
                    <x-button class="w-full text-white bg-green-600 hover:bg-green-700 hover:shadow-green-700/50">Register</x-button>
                </div>
            </form>

            <!-- Or -->
            <x-divider-h text="OR"></x-divider-h>

            <x-parts-social-media-buttons></x-parts-social-media-buttons>

            <!-- Register link -->
            <div class="text-sm text-slate-900 dark:text-slate-500 mt-4">
                Already have an account? <a href="/pages/auth/login"
                                            class="text-blue-600 hover:underline">Login</a>
            </div>
        </div>
    </main>
@endsection
