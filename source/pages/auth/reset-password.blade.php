@extends('_layouts.auth')

@section('body')
    <main>
        <div class="h-1 bg-slate-400 dark:bg-slate-700 rounded-t-lg mt-4"></div>
        <div class="w-full max-w-sm px-4 py-6 bg-white rounded-b-lg dark:bg-slate-900">
            <h1 class="text-xl font-semibold text-center text-slate-900 dark:text-slate-200">Reset password</h1>
            <br>
            <p class="text-sm text-center text-gray-500 dark:text-gray-400">
                You are only one step a way from your new password, recover your password now.
            </p>
            <form action="#" class="mt-4">
                <x-input type="email" placeholder="E-mail address" required=""></x-input>
                <x-input type="password" placeholder="Password" minlength="8" required=""></x-input>
                <x-input type="password" placeholder="Confirm Password" minlength="8" required=""></x-input>
                <div>
                    <x-button class="w-full text-white bg-green-600 hover:bg-green-700 hover:shadow-green-700/50"
                              >Reset password</x-button>
                </div>
            </form>
            <!-- Register link -->
            <div class="text-sm text-slate-900 dark:text-slate-500 mt-4">
                <a href="/pages/auth/login"
                   class="text-blue-600 hover:underline">Remember your username and password?</a>
            </div>
        </div>
    </main>
@endsection
