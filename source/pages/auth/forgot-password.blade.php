@extends('_layouts.auth')

@section('body')
    <main>
        <div class="h-1 bg-slate-400 dark:bg-slate-700 rounded-t-lg mt-4"></div>
        <div class="w-full max-w-sm px-4 py-6 bg-white rounded-b-lg dark:bg-slate-900">
            <p class="text-sm text-center text-gray-500 dark:text-gray-400">
                You forgot your password? Here you can easily retrieve a new password.
            </p>
            <form action="#" class="mt-4">
                <x-input type="email" placeholder="E-mail address" required=""></x-input>
                <div>
                    <x-button class="w-full text-white bg-green-600 hover:bg-green-700 hover:shadow-green-700/50">Request new password</x-button>
                </div>
            </form>

            <!-- Register link -->
            <div class="text-sm text-slate-900 dark:text-slate-500 mt-4">
                <a href="/pages/auth/reset-password"
                   class="text-blue-600 hover:underline">Reset password</a>
            </div>
        </div>
    </main>
@endsection
