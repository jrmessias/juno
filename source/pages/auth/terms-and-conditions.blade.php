@extends('_layouts.empty')

@section('body')
    <div class="flex flex-col items-center justify-center min-h-screen space-y-4 antialiased text-gray-900 bg-gradient-to-t from-slate-300 to-slate-100 dark:from-slate-800 dark:to-slate-700/50 dark:bg-slate-900 dark:text-slate-400 transition font-sans text-justify">
        <br>
        <x-brand></x-brand>
        <br>
        <div class="max-w-screen-md">

            <h1 class="text-2xl font-bold text-center">Generic Terms and Conditions Template</h1><br>
            <p>Please read these terms and conditions ("terms and conditions", "terms") carefully before using [website]
                website ("website", "service") operated by [name] ("us", 'we", "our").</p>
            <br>
            <h2 class="text-lg font-bold">Conditions of use</h2>
            <p>By using this website, you certify that you have read and reviewed this Agreement and that you agree to
                comply with its terms. If you do not want to be bound by the terms of this Agreement, you are advised to
                leave the website accordingly. [name] only grants use and access of this website, its products, and its
                services to those who have accepted its terms.</p>
            <br>
            <h2 class="text-lg font-bold">Privacy policy</h2>
            <p>Before you continue using our website, we advise you to read our privacy policy [link to privacy policy]
                regarding our user data collection. It will help you better understand our practices.</p>
            <br>
            <h2 class="text-lg font-bold">Age restriction</h2>
            <p>You must be at least 18 (eighteen) years of age before you can use this website. By using this website,
                you warrant that you are at least 18 years of age and you may legally adhere to this Agreement. [name]
                assumes no responsibility for liabilities related to age misrepresentation.</p><br>
            <h2 class="text-lg font-bold">Intellectual property</h2>
            <p>You agree that all materials, products, and services provided on this website are the property of [name],
                its affiliates, directors, officers, employees, agents, suppliers, or licensors including all
                copyrights, trade secrets, trademarks, patents, and other intellectual property. You also agree that you
                will not reproduce or redistribute the [name]&rsquo;s intellectual property in any
                way, including electronic, digital, or new trademark registrations.</p><br>
            <p>You grant [name] a royalty-free and non-exclusive license to display, use, copy, transmit, and broadcast
                the content you upload and publish. For issues regarding intellectual property claims, you should
                contact the company in order to come to an agreement.</p><br>
            <h2 class="text-lg font-bold">User accounts</h2>
            <p>As a user of this website, you may be asked to register with us and provide private information. You are
                responsible for ensuring the accuracy of this information, and you are responsible for maintaining the
                safety and security of your identifying information. You are also responsible for all activities that
                occur under your account or password.</p><br>
            <p>If you think there are any possible issues regarding the security of your account on the website, inform
                us immediately so we may address them accordingly.</p><br>
            <p>We reserve all rights to terminate accounts, edit or remove content and cancel orders at our sole
                discretion.</p><br>
            <h2 class="text-lg font-bold">Applicable law</h2>
            <p>By visiting this website, you agree that the laws of the [location], without regard to principles of
                conflict laws, will govern these terms and conditions, or any dispute of any sort that might come
                between [name] and you, or its business partners and associates.</p><br>
            <h2 class="text-lg font-bold">Disputes</h2>
            <p>Any dispute related in any way to your visit to this website or to products you purchase from us shall be
                arbitrated by state or federal court [location] and you consent to exclusive jurisdiction and venue of
                such courts.</p><br>
            <h2 class="text-lg font-bold">Indemnification</h2>
            <p>You agree to indemnify [name] and its affiliates and hold [name] harmless against legal claims and
                demands that may arise from your use or misuse of our services. We reserve the right to select our own
                legal counsel.</p><br>
            <h2 class="text-lg font-bold">Limitation on liability</h2>
            <p>[name] is not liable for any damages that may occur to you as a result of your misuse of our website.</p>
            <p>[name] reserves the right to edit, modify, and change this Agreement at any time. We shall let our users
                know of these changes through electronic mail. This Agreement is an understanding between [name] and the
                user, and this supersedes and replaces all prior agreements regarding the use of this website.</p>
            <br>
            <p>Terms and conditions
                template by
                <a href="https://www.websitepolicies.com/"
                   class="text-sm text-blue-600 hover:underline">WebsitePolicies.com</a>.</p>
            <br>

        </div>
    </div>
@endsection
