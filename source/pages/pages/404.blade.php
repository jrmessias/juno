@extends('_layouts.empty')

@section('body')
    <div
            class="
    flex
    items-center
    justify-center
    w-screen
    h-screen
    bg-gradient-to-t
    from-slate-200
    to-slate-100
    dark:from-slate-900
    dark:to-slate-800
  "
    >
        <div class="px-40 py-20 bg-white dark:bg-slate-800 rounded-lg shadow-xl">
            <div class="flex flex-col items-center">
                <h1 class="font-bold text-blue-600 text-9xl">404</h1>

                <h6 class="mb-2 text-2xl font-bold text-center text-slate-800 dark:text-slate-500 md:text-3xl">
                    <span class="text-red-500">Oops!</span> Page not found.
                </h6>

                <p class="mb-8 text-center text-slate-500 md:text-lg">
                    The page you’re looking for doesn’t exist.
                </p>

                <a
                        href="/"
                        class="px-6 py-2 text-sm font-semibold text-blue-800 bg-blue-100 dark:text-blue-200 dark:bg-blue-800 hover:bg-blue-200 dark:hover:bg-blue-900 rounded-lg"
                >Go home</a
                >
            </div>
        </div>
    </div>
@endsection
