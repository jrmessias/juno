<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Auth {{ $page->description ?? $page->siteDescription }}">
    <meta property="og:site_name" content="{{ $page->siteName }}"/>
    <meta property="og:title" content="{{ $page->title ?  $page->title . ' | ' : '' }}{{ $page->siteName }}"/>
    <meta property="og:description" content="{{ $page->description ?? $page->siteDescription }}"/>
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:image" content="/assets/img/logo.png"/>
    <meta property="og:type" content="website"/>
    <meta name="twitter:image:alt" content="{{ $page->siteName }}">
    <meta name="twitter:card" content="summary_large_image">
    <title>{{ $page->siteName }}{{ $page->title ? ' | ' . $page->title : '' }}</title>
    <link rel="home" href="{{ $page->baseUrl }}">
    <link rel="icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/main.css') }}">
{{--    <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">--}}
</head>
<body>
<!-- Setup -->
<div x-data="boot()" x-init="$refs.loading.classList.add('hidden');" :class="{ 'dark': isDark}" class="transition">

                    @yield('body')
        <x-toggle-theme></x-toggle-theme>
    <!-- / Body -->
</div>
<!-- / Setup -->
@stack('scripts')
<script src="{{ mix('js/main.js') }}" defer></script>
<script>
    const boot = () => {
        const getTheme = () => {
            if (window.localStorage.getItem('theme')) {
                return JSON.parse(window.localStorage.getItem('theme'))
            }

            return !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
        }

        const setTheme = (value) => {
            window.localStorage.setItem('theme', value)
        }

        const getSidebar = () => {
            return true;//window.localStorage.getItem('sideBarOpen')
        }

        const setSidebar = (value) => {
            window.localStorage.setItem('sideBarOpen', value)
        }

        return {
            loading: true,
            isDark: getTheme(),
            isSidebarOpen: getSidebar(),
            toggleTheme() {
                this.isDark = !this.isDark
                setTheme(this.isDark)
            },
            setLightTheme() {
                this.isDark = false
                setTheme(this.isDark)
            },
            setDarkTheme() {
                this.isDark = true
                setTheme(this.isDark)
            },
            toggleSidebar() {
                this.isSidebarOpen = !this.isSidebarOpen
                setSidebar(!this.isSidebarOpen);
            },
            // isSettingsPanelOpen: false,
            // openSettingsPanel() {
            //     this.isSettingsPanelOpen = true
            //     this.$nextTick(() => {
            //         this.$refs.settingsPanel.focus()
            //     })
            // },
            // isNotificationsPanelOpen: false,
            // openNotificationsPanel() {
            //     this.isNotificationsPanelOpen = true
            //     this.$nextTick(() => {
            //         this.$refs.notificationsPanel.focus()
            //     })
            // },
            // isSearchPanelOpen: false,
            // openSearchPanel() {
            //     this.isSearchPanelOpen = true
            //     this.$nextTick(() => {
            //         this.$refs.searchInput.focus()
            //     })
            // },
            isMobileSubMenuOpen: false,
            openMobileSubMenu() {
                this.isMobileSubMenuOpen = true
                this.$nextTick(() => {
                    this.$refs.mobileSubMenu.focus()
                })
            },
            isMobileMainMenuOpen: false,
            openMobileMainMenu() {
                this.isMobileMainMenuOpen = true
                this.$nextTick(() => {
                    this.$refs.mobileMainMenu.focus()
                })
            },
        }
    }
</script>
</body>
</html>
