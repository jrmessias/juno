@extends('_layouts.master')

@section('body')

    <!-- Header title -->
    @include("_partials.content-title", ["title" => "Dashboard"])

    <!-- Content -->
@endsection

@section('panel-right')
    <h3 class="p-4">Right panel</h3>
@endsection

@section('panel-left')
    <h3 class="p-4">Left panel</h3>
@endsection
