<!-- Panel -->
<!-- Backdrop -->
<div
        x-transition:enter="transition duration-300 ease-in-out"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition duration-300 ease-in-out"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        x-show="{{ $control }}"
        @click="{{ $control }} = false"
        class="fixed inset-0 z-10 bg-slate-500/30 dark:bg-slate-900/90 backdrop-blur-sm transition"
        aria-hidden="true"
></div>
<!-- Panel -->
<section
        x-transition:enter="transition duration-300 ease-in-out transform sm:duration-500"
        x-transition:enter-start="{{ isset($right) ? '' : '-' }}translate-x-full"
        x-transition:enter-end="translate-x-0"
        x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500"
        x-transition:leave-start="translate-x-0"
        x-transition:leave-end="{{ isset($right) ? '' : '-' }}translate-x-full"
        tabindex="-1"
        x-show="{{ $control }}"
        @keydown.escape="{{ $control }} = false"
        class="fixed inset-y-0 {{ isset($right)  ? 'right-0' : '' }} z-20 w-full max-w-xs bg-slate-100 shadow-xl dark:bg-slate-900 dark:text-slate-300 sm:max-w-md focus:outline-none">

    <!-- Close button -->
    <div class="absolute {{ isset($right) ? 'left-0' : 'right-0' }} p-2 transform {{ isset($right) ? '-translate-x-full' : 'translate-x-full' }}">
        <button @click="{{ $control }} = false" class="p-2 rounded-md focus:outline-none">
            <svg
                    class="w-5 h-5 fill-slate-700 dark:fill-slate-200"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
            >
                <path d="m16.192 6.344-4.243 4.242-4.242-4.242-1.414 1.414L10.535 12l-4.242 4.242 1.414 1.414 4.242-4.242 4.243 4.242 1.414-1.414L13.364 12l4.242-4.242z"></path>
            </svg>
        </button>
    </div>

    <h2 class="sr-only">{{ isset($right) ? 'Right' : 'Left' }} panel</h2>
    <!-- Panel content -->
    <div class="flex flex-col h-screen">
        <!-- Panel content -->
        {{ $slot ?? "" }}
    </div>
</section>
