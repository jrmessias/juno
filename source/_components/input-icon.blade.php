<div class="relative">
    <div class="text-gray-300 absolute ml-4 inset-0 m-auto h-4">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" class="fill-slate-500">
        {{ $slot }}
        </svg>
    </div>
    <input class="peer w-full px-4 py-2 pl-10 border border-slate-400 rounded-lg
 dark:text-slate-400 dark:bg-slate-900 dark:border-slate-700
  focus:outline-none focus:ring-0 focus:border-slate-700 focus:invalid:border-red-300 focus:invalid:text-red-500
  placeholder:text-slate-500
{{--  invalid:border-red-500 invalid:text-red-500 dark:invalid:border-red-500 dark:invalid:text-red-500--}}
            valid:border-green-500
            autofill:bg-yellow-500 mb-4 {{ $class ?? "" }}"
           type="{{ $type ?? "text" }}" name="{{ $name ?? "name" }}" minlength="{{ $minlength ?? "" }}" id="{{ $id ?? $name ?? "" }}"
           placeholder="{{ $placeholder ?? "" }}" {{ isset($required) ? 'required=""' : ""}}>
</div>
