<div class="p-4 m-4 bg-blue-200 rounded-lg dark:bg-blue-900 transition-all duration-200 ease-in-out self-end" role="alert" x-data="{ cta: true }" x-show="cta">
    <div class="flex items-center mb-3">
        <span class="bg-green-100 text-green-800 text-sm font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-green-200 dark:text-green-900">New!</span>
        <button type="button" class="ml-auto -mx-1.5 -my-1.5 text-blue-900 rounded-lg p-1 hover:bg-blue-300 inline-flex h-6 w-6 dark:bg-blue-900 dark:text-blue-400 dark:hover:bg-blue-800" @click="cta = ! cta" data-collapse-toggle="dropdown-cta" aria-label="Close">
            <span class="sr-only">Close</span>
            <svg class="w-4 h-4" fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="m16.192 6.344-4.243 4.242-4.242-4.242-1.414 1.414L10.535 12l-4.242 4.242 1.414 1.414 4.242-4.242 4.243 4.242 1.414-1.414L13.364 12l4.242-4.242z"></path>
            </svg>
        </button>
    </div>
    <p class="mb-3 text-sm text-blue-900 dark:text-blue-400">
        New feature! To use click in confirm.
    </p>
    <a class="text-sm text-blue-900 underline hover:text-blue-800 dark:text-blue-400 dark:hover:text-blue-300" href="#">Confirm use new feature</a>
</div>
