<button type="submit"
        class="px-4 py-2 font-medium text-center transition-colors duration-200 rounded-md focus:outline-none shadow-md {{ $class }}">
    {{ $slot  }}
</button>

