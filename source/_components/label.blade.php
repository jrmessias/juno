<label class="block text-sm font-medium text-slate-700 dark:text-slate-500 mb-1" for="{{ $for ?? "" }}">{{ $label ?? "" }}</label>

