<button type="submit"
        class="px-6 py-4 font-medium text-center transition-colors duration-200 rounded-md focus:outline-none shadow-md {{ $class }}">
    {{ $slot  }}
</button>

