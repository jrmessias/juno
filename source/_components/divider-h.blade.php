<div class="flex items-center justify-center space-x-2 flex-nowrap mt-4 mb-4">
    <span class="w-20 h-px bg-slate-300 dark:bg-slate-700"></span>
    <span class="text-slate-700 dark:text-slate-400">{{ $text }}</span>
    <span class="w-20 h-px bg-slate-300 dark:bg-slate-700"></span>
</div>
