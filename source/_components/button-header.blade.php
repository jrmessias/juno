<button
        @click="{{ $click ?? "" }}"
        class="p-2 transition-colors duration-200 rounded-full bg-slate-200/50 hover:bg-slate-200 dark:bg-slate-700/50 dark:hover:bg-slate-700 focus:outline-none"
>
    <span class="sr-only">{{ $title ?? "" }}</span>
    {{ $slot }}
</button>
