<div class="inline-block mb-6 text-3xl font-bold tracking-wider w-48 h-5 mb-2">
    <svg xmlns="http://www.w3.org/2000/svg" width="160.483" height="159.863" class="w-full"
         :class="isDark ? 'fill-slate-300' : 'fill-slate-700'">
        <path d="M2690 14c902 256 1563 1086 1563 2070 0 1188-963 2152-2152 2152-1007 0-1853-692-2087-1626 187 53 385 82 589 82 1188 0 2152-963 2152-2152 0-181-23-357-65-525z"/>
    </svg>
</div>
