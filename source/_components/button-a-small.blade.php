<a href="{{ $href ?? "#" }}"
   class="items-right justify-center inline-block px-2 py-1 space-x-2 transition-all duration-200 rounded-md focus:outline-none shadow-md {{ $class }}">
    <div class="flex items-center justify-center">
    {{ $slot }}
    </div>
</a>
