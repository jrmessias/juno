<button type="submit"
        class="px-2 py-1 font-medium text-center transition-colors duration-200 rounded-md focus:outline-none shadow-md {{ $class }}">
    {{ $slot  }}
</button>

