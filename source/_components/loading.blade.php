<!-- Loading screen -->
<div
    x-ref="loading"
    class="fixed inset-0 z-50 flex items-center justify-center text-2xl font-sans font-semibold text-white bg-primary-darker"
>
    Loading.....
</div>
<!-- / Loading screen -->
