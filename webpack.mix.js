const mix = require('laravel-mix');
require('laravel-mix-jigsaw');

mix.disableSuccessNotifications();
mix.setPublicPath('source/assets');
//

// .jigsaw({
//     watch: [
//         'config.php',
//         "./source/index.blade.php",
//         "./source/pages/**/*.blade.php",
//         "./source/_layouts/*.blade.php",
//         "./source/_assets/css/*.css",
//         "./source/_assets/js/*.js"
//     ],
// })

mix.jigsaw()
    .copyDirectory("node_modules/@mdi/font/fonts/**", "./source/assets/fonts/")
    .js("source/_assets/js/main.js", "js")
    .js("source/_assets/js/auth.js", "js")
    .postCss("source/_assets/css/main.css", "css", [
        require("postcss-import"),
        require("tailwindcss/nesting"),
        require("tailwindcss"),
        require("autoprefixer"),
    ])
    .postCss("source/_assets/css/vendor.css", "css", [
        require("postcss-import"),
    ])
    .options({
        processCssUrls: false,
    })
    .sourceMaps()
    .version();
