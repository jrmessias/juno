<?php

return [
    'baseUrl' => '',
    'production' => true,
    'siteName' => 'owl Admin Dashboard Template',
    'siteDescription' => 'powered by Jigsaw',
    'github' => 'jrmessias',
    'id' => 'JRM',
    'author' => 'Júnior Messias',
    'menuUser' => [
        ["link" => "profile", "name" => "Profile"],
        ["link" => "logout", "name" => "Logout"],
    ],

    'menuSidebar' => [
        [
            "title" => "CRUD",
            "itens" => [
                ["url" => "/pages/crud/create", "title" => "Create"],
                ["url" => "/pages/crud/list", "title" => "List"],
            ],
            "icon" => "<path d=\"M8.707 19.707 18 10.414 13.586 6l-9.293 9.293a1.003 1.003 0 0 0-.263.464L3 21l5.242-1.03c.176-.044.337-.135.465-.263zM21 7.414a2 2 0 0 0 0-2.828L19.414 3a2 2 0 0 0-2.828 0L15 4.586 19.414 9 21 7.414z\"></path>",
        ],
        [
            "title" => "Pages",
            "itens" => [
                ["url" => "/pages/pages/blank", "title" => "Blank"],
                ["url" => "/pages/pages/404", "title" => "404"],
                ["url" => "/pages/pages/500", "title" => "500"],
            ],
            "icon" => "<path d=\"M19.937 8.68c-.011-.032-.02-.063-.033-.094a.997.997 0 0 0-.196-.293l-6-6a.997.997 0 0 0-.293-.196c-.03-.014-.062-.022-.094-.033a.991.991 0 0 0-.259-.051C13.04 2.011 13.021 2 13 2H6c-1.103 0-2 .897-2 2v16c0 1.103.897 2 2 2h12c1.103 0 2-.897 2-2V9c0-.021-.011-.04-.013-.062a.99.99 0 0 0-.05-.258zM16.586 8H14V5.414L16.586 8zM6 20V4h6v5a1 1 0 0 0 1 1h5l.002 10H6z\"></path>",

        ],
        [
            "title" => "Authentication",
            "itens" => [
                ["url" => "/pages/auth/register", "title" => "Register"],
                ["url" => "/pages/auth/login", "title" => "Login"],
                ["url" => "/pages/auth/forgot-password", "title" => "Forgot Password"],
                ["url" => "/pages/auth/reset-password", "title" => "Reset Password"],
                ["url" => "/pages/auth/terms-and-conditions", "title" => "Terms and conditions"],
            ],
            "icon" => "<path d=\"M12 2a5 5 0 1 0 5 5 5 5 0 0 0-5-5zm0 8a3 3 0 1 1 3-3 3 3 0 0 1-3 3zm9 11v-1a7 7 0 0 0-7-7h-4a7 7 0 0 0-7 7v1h2v-1a5 5 0 0 1 5-5h4a5 5 0 0 1 5 5v1z\"></path>",
        ],
    ],
    // helpers
    'isActive' => function ($page, $path) {
        return Str::endsWith(trimPath($page->getPath()), trimPath($path));
    },
    'isActiveParent' => function ($page, $menuItem) {
        if (is_object($menuItem) && $menuItem->children) {
            return $menuItem->children->contains(function ($child) use ($page) {
                return trimPath($page->getPath()) == trimPath($child);
            });
        }
        return "";
    },
    'url' => function ($page, $path) {
        return Str::startsWith($path, 'http') ? $path : '/' . trimPath($path);
    },
];
