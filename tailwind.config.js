const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'class',
  content: [
    "./source/index.blade.php",
    "./source/_assets/css/*.css",
    "./source/_assets/js/*.js",
    "./source/_assets/fonts/*",
    "./source/_components/*.blade.php",
    "./source/_layouts/*.blade.php",
    "./source/_partials/*.blade.php",
    "./source/pages/**/*.blade.php"
  ],
  theme: {
    extend: {
      fontFamily: {
        display: ['Inter', 'system-ui', 'sans-serif'],
        body: ['Inter', 'system-ui', 'sans-serif'],
      },
      colors: {
        current: 'currentColor',
        green: colors.emerald,
        yellow: colors.amber,
        purple: colors.violet,
        gray: colors.neutral,
      }
    },
  },
  corePlugins: {
    aspectRatio: false,
  },
  plugins:[
    require('@tailwindcss/typography'),
    require("@tailwindcss/forms")({
      strategy: 'base', // only generate global styles
      // strategy: 'class', // only generate classes
    }),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
    require('prettier-plugin-tailwindcss'),
  ]
};
